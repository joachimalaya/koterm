import org.joachimalaya.koterm.formatting.AnsiFormatting
import org.joachimalaya.koterm.formatting.BackgroundColor
import org.joachimalaya.koterm.formatting.ForegroundColor
import org.joachimalaya.koterm.formatting.FormattedText
import org.joachimalaya.koterm.layout.AnsiLists
import org.junit.Test

class DevelopmentTest {

    @Test
    fun developmentTest() {
        val items = listOf("bla", "bli", "blub")

        println(AnsiLists.orderedList(items))

        println(AnsiLists.unorderedList(items))

        println(ForegroundColor.GREEN(ForegroundColor.BLUE("blue") + " and green"))

        println(FormattedText(listOf(FormattedText("blue", ForegroundColor.BLUE), " and green"), ForegroundColor.GREEN))

        val pretty = BackgroundColor.BRIGHT_BLUE(ForegroundColor.BRIGHT_MAGENTA("all pretty"))
        println(pretty)

        println(AnsiFormatting.ITALIC("italic"))

        println(AnsiFormatting.BOLD("bold"))

        val boldItalic = AnsiFormatting.ITALIC(AnsiFormatting.BOLD("bold and italic"))
        println(boldItalic)

        val coloredText = ForegroundColor.BLUE("blue ") + ForegroundColor.GREEN("green")
        println(coloredText)

        val formattedText = AnsiFormatting.FRAMED("framed ") + AnsiFormatting.ENCIRCLED("encircled ") + AnsiFormatting.OVERLINED("overlined")
        println(formattedText)

        println(AnsiFormatting.FRAMED("framed"))
        println(AnsiFormatting.OVERLINED("overlined"))
        println(AnsiFormatting.ENCIRCLED("encircled"))
    }
}