package org.joachimalaya.koterm.layout.table

import org.junit.Test

class AnsiTablesTest {

    @Test
    fun printDoubleColumn() {
        AnsiTables.print(TableData(
                listOf(
                        ColumnData("first", listOf("abc", "def", "ghi")),
                        ColumnData("second", listOf("bla", "bli", "blub")))))
    }

    @Test
    fun printSingleColumn() {
        AnsiTables.print(TableData(
                listOf(
                        ColumnData("first", listOf("abc", "def", "ghi")))))
    }

    @Test
    fun printTripleColumn() {
        AnsiTables.print(TableData(
                listOf(
                        ColumnData("first", listOf("abc", "def", "ghi")),
                        ColumnData("second", listOf("abc", "def", "ghi")),
                        ColumnData("third", listOf("abc", "def", "ghi"))
                )))
    }
}