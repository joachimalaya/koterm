package org.joachimalaya.koterm.interaction

import org.joachimalaya.koterm.layout.AnsiLists
import java.util.*

/**
 * Functions to let a user choose an item from a list.
 * By displaying the choices on the terminal and waiting for an answer on standard input.
 */
object ListChoice {

    /**
     * Let user choose an item from a given list. Blocks execution.
     */
    fun <C : Any> poseChoice(choices: List<C>): C {
        println(AnsiLists.orderedList(choices))
        return choices[waitForIndexAnswer(choices.size)]
    }

    /**
     * Let user choose an item from a given list.
     * After user made their choice, call callback with chosen item as parameter.
     */
    fun <C : Any> asyncPoseChoice(choices: List<C>, callback: (C) -> Unit) {
        Thread {
            callback(poseChoice(choices))
        }.start()
    }

    private fun waitForIndexAnswer(maxIndex: Int): Int {
        var answer = -1
        while (answer !in 1..maxIndex) {
            try {
                val scanner = Scanner(System.`in`)
                print("Enter a choice number: ")
                answer = scanner.nextInt()
            } catch (ime: InputMismatchException) {
                println("Please enter only integers greater than 0")
            }
        }

        return answer - 1
    }

}