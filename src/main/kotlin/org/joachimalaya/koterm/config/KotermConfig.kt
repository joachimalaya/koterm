package org.joachimalaya.koterm.config

/**
 * Central config object for library wide settings.
 */
object KotermConfig {

    /**
     * The terminal width in characters at program start.
     * Is used to calculate output sizes.
     *
     * Read from environment variable COLUMNS if set. Defaults to 80.
     *
     * COLUMNS is set automatically on most Linux terminals.
     */
    var TERMINAL_WIDTH = System.getenv("COLUMNS")?.toInt() ?: 80
        private set

    /**
     * The terminal height in characters at program start.
     * Is used to calculate output sizes.
     *
     * Read from environment variable ROWS if set. Defaults to 80.
     *
     * ROWS is set automatically on most Linux terminals.
     */
    var TERMINAL_HEIGHT = System.getenv("ROWS")?.toInt() ?: 24
        private set
}
