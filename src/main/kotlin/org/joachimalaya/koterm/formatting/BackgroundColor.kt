package org.joachimalaya.koterm.formatting

/**
 * A collection of background-colors for text, that are supported on most ANSI-capable terminals.
 *
 * color codes s. https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
 */
enum class BackgroundColor(private val colorString: String) : Formatting {

    BLACK("40m"),
    RED("41m"),
    GREEN("42m"),
    YELLOW("43m"),
    BLUE("44m"),
    CYAN("46m"),
    WHITE("47m"),
    DEFAULT("49m"),
    BRIGHT_BLACK("100m"),
    BRIGHT_RED("101m"),
    BRIGHT_GREEN("102m"),
    BRIGHT_YELLOW("103m"),
    BRIGHT_BLUE("104m"),
    BRIGHT_MAGENTA("105m"),
    BRIGHT_CYAN("106m"),
    BRIGHT_WHITE("107m");

    override fun format(text: String): String {
        return "$ANSI_FORMATTING_ESCAPE$colorString$text$ANSI_FORMATTING_ESCAPE${DEFAULT.colorString}"
    }
}