package org.joachimalaya.koterm.formatting

/**
 * A collection of text-colors, that are supported on most ANSI-capable terminals.
 *
 * color codes s. https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
 */
enum class ForegroundColor(private val colorString: String) : Formatting {

    BLACK("30m"),
    RED("31m"),
    GREEN("32m"),
    YELLOW("33m"),
    BLUE("34m"),
    MAGENTA("35m"),
    CYAN("36m"),
    WHITE("37m"),
    DEFAULT("39m"),
    BRIGHT_BLACK("90m"),
    BRIGHT_RED("91m"),
    BRIGHT_GREEN("92m"),
    BRIGHT_YELLOW("93m"),
    BRIGHT_BLUE("94m"),
    BRIGHT_MAGENTA("95m"),
    BRIGHT_CYAN("96m"),
    BRIGHT_WHITE("97m");

    override fun format(text: String): String {
        return "$ANSI_FORMATTING_ESCAPE$colorString$text$ANSI_FORMATTING_ESCAPE${DEFAULT.colorString}"
    }
}