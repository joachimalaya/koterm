package org.joachimalaya.koterm.formatting

/**
 * Data structure that handles complex formatting information.
 *
 * As developer using this library you might not need to interact with this class directly,
 * because formatting is applied to the text when calling toString() which is implicit in most use cases.
 */
class FormattedText(private val textSnippets: List<CharSequence>, private val formatting: Formatting) : CharSequence {

    constructor(textSnippet: CharSequence, formatting: Formatting) : this(listOf(textSnippet), formatting)

    override val length: Int
        get() = textSnippets.sumBy { it.length }

    override fun get(index: Int): Char {
        var snippetLengthSum = 0
        var snippetIndex = 0
        while (snippetLengthSum + textSnippets[snippetIndex].length < index) {
            snippetLengthSum += textSnippets[snippetIndex++].length
        }

        return textSnippets[snippetIndex][index - snippetLengthSum]
    }

    override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
        var snippetLengthSum = 0
        var snippetIndex = 0
        val subSequenceLength = endIndex - startIndex

        while (snippetLengthSum + textSnippets[snippetIndex].length < startIndex) {
            snippetLengthSum += textSnippets[snippetIndex++].length
        }

        var result = ""
        var inSnippetIndex = startIndex - snippetLengthSum
        while (result.length < subSequenceLength) {
            result += textSnippets[snippetIndex][inSnippetIndex++]
            if (snippetIndex >= textSnippets[snippetIndex].length) {
                snippetIndex++
                inSnippetIndex = 0
            }
        }

        return result
    }

    operator fun plus(other: CharSequence): FormattedText {
        return FormattedText(textSnippets + other, formatting)
    }

    operator fun CharSequence.plus(formattedOther: FormattedText): FormattedText {
        return FormattedText(listOf(this, *formattedOther.textSnippets.toTypedArray()), formattedOther.formatting)
    }

    override fun toString(): String {
        return textSnippets.asSequence()
                .map(CharSequence::toString)
                .map(formatting::format)
                .reduce(String::plus)
    }

}