package org.joachimalaya.koterm.formatting

/**
 * Style your text in ways, that do not change the color.
 */
enum class AnsiFormatting(private val formatting: String, private val formattingOff: String) : Formatting {

    BOLD("1m", "21m"),
    ITALIC("3m", "23m"),
    UNDERLINE("4m", "24m"),

    // below rarely supported
    FRAMED("51m", "54m"),
    ENCIRCLED("52m", "54m"),
    OVERLINED("53m", "55m");

    override fun format(text: String): String {
        return "$ANSI_FORMATTING_ESCAPE$formatting$text$ANSI_FORMATTING_ESCAPE$formattingOff"
    }

}