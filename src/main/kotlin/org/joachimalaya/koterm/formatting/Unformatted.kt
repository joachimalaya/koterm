package org.joachimalaya.koterm.formatting

enum class Unformatted : Formatting {

    /**
     * Mark a text as unformatted.
     */
    UNFORMATTED;

    override fun format(text: String): String {
        return text
    }
}