package org.joachimalaya.koterm.formatting

const val ANSI_FORMATTING_ESCAPE = "\u001B["

/**
 * Formatting can change CharSequences so they will displayed formatted when written to a terminal.
 */
interface Formatting {

    operator fun invoke(text: CharSequence): FormattedText {
        return FormattedText(text, this)
    }

    fun format(text: String): String

}