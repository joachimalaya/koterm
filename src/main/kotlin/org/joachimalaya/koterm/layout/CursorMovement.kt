package org.joachimalaya.koterm.layout

import org.joachimalaya.koterm.formatting.ANSI_FORMATTING_ESCAPE

const val PREVIOUS_LINE = "${ANSI_FORMATTING_ESCAPE}1F"
const val NEXT_LINE = "${ANSI_FORMATTING_ESCAPE}1E"
const val START_OF_LINE = "${ANSI_FORMATTING_ESCAPE}1G"