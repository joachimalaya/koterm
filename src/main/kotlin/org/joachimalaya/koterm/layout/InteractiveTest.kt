package org.joachimalaya.koterm.layout

import org.joachimalaya.koterm.display.ProgressBar
import org.joachimalaya.koterm.interaction.ListChoice

/**
 * This executable is only used for development purposes.
 *
 * This is necessary, as some of the formatting and outputs are not properly displayed in IDEs.
 * Thus features need to be tested in a proper terminal.
 */
fun main(args: Array<String>) {
    val items = listOf("bla", "bli", "blub", "a", "bla", "bli", "blub", "a", "bla", "bli", "blub", "a", "bla", "bli", "blub", "a")

    val chosen = ListChoice.poseChoice(items)
    println("item $chosen was chosen")

    val progressBar = ProgressBar(0, 100)
    for (i in 0..100) {
        progressBar.update(i)
        Thread.sleep(200)
    }

    println("\n")
}