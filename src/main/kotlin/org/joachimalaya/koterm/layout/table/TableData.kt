package org.joachimalaya.koterm.layout.table


/**
 * Holds tabular data of arbitrary type. Can be printed to console with [AnsiTables].
 */
class TableData<T>(val columns: List<ColumnData<T>>) {

    fun row(i: Int): List<T?> {
        return columns.map { it.values.getOrNull(i) }
    }

}