package org.joachimalaya.koterm.layout.table

/**
 * Print tabular data to the console.
 */
object AnsiTables {

    /**
     * Print the given [TableData] as a table to the console. To find the String representation for the entries toString() is called.
     */
    fun <T : Any> print(tableData: TableData<T>) {
        print(tableData) { it.toString() }
    }

    /**
     * Print the given [TableData] as a table to the console. To create the String for an entry the caller may give a callback function.
     */
    fun <T> print(tableData: TableData<T>, callback: (T) -> String) {
        val rows = tableData.columns.maxBy { it.values.size }?.values?.size ?: 0
        println(tableData.columns.joinToString(" | ") { it.header.padEnd(it.columnWidth()) })
        println(tableData.columns.joinToString("-+-") { "-".repeat(it.columnWidth()) })

        for (i in 0 until rows) {
            println(tableData.row(i)
                    .map { if (it == null) "" else callback(it) }
                    .mapIndexed { columnIndex, cell -> cell.padEnd(tableData.columns[columnIndex].columnWidth()) }
                    .joinToString(" | "))
        }
    }
}