package org.joachimalaya.koterm.layout.table

import kotlin.math.max

/**
 * Holds the data of a column of [TableData].
 */
class ColumnData<T>(val header: String, val values: List<T>) {

    /**
     * Find the length of the longest entry (by toString()) in this column.
     */
    fun columnWidth(): Int {
        return max(values.maxBy {
            it.toString()
                    .length
        }.toString().length, header.length)
    }
}