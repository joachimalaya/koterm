package org.joachimalaya.koterm.layout

import org.joachimalaya.koterm.formatting.ForegroundColor

/**
 * Convenience functions to display lists of objects in a terminal.
 */
object AnsiLists {

    /**
     * Prints out all items with numbers starting with 1.
     */
    fun orderedList(items: List<Any>): String {
        var index = 1
        val maxIndexLength = items.size.toString()
                .length
        return items.joinToString("\n", transform = {
            val paddedNumber = index++.toString()
                    .padStart(maxIndexLength, ' ')
            "\t${ForegroundColor.GREEN(paddedNumber)}. $it"
        })
    }

    /**
     * Prints out all items with * as item indicator.
     */
    fun unorderedList(items: List<Any>): String {
        return items.joinToString("\n", transform = { "\t* $it" })
    }

}