package org.joachimalaya.koterm.display

import org.joachimalaya.koterm.config.KotermConfig
import org.joachimalaya.koterm.formatting.ForegroundColor
import org.joachimalaya.koterm.layout.PREVIOUS_LINE

/**
 * A progress indicator, that can display the progress of a longer running process in a terminal.
 */
class ProgressBar(private var currentValue: Int, private var maxValue: Int, private val completedChar: Char = '#', private val todoChar: Char = '-', private var statusText: String = "", private val displayStatus: Boolean = true) {

    private fun draw() {
        val charsAvailable = KotermConfig.TERMINAL_WIDTH - 2
        val charsDone = (charsAvailable * (currentValue / maxValue.toFloat())).toInt()

        val completedBar = ForegroundColor.GREEN(completedChar.toString()
                .repeat(charsDone))
        val todoBar = todoChar.toString()
                .repeat(charsAvailable - charsDone)

        println("[$completedBar$todoBar]")
        if (displayStatus) {
            val paddedCurrentValue = currentValue.toString()
                    .padStart(maxValue.toString().length, ' ')
            print("$paddedCurrentValue / $maxValue $statusText$PREVIOUS_LINE")
        } else {
            print(PREVIOUS_LINE)
        }
    }

    /**
     * Update the value indicating the amount of work done and redraw the progress bar in terminal.
     *
     * Also supports changing the status text displayed below the bar.
     */
    fun update(newValue: Int, newStatus: String = statusText) {
        currentValue = newValue
        statusText = newStatus
        draw()
    }
}