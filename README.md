# koterm
Terminal text styling and user interaction for Kotlin. Inspired by chalk.js

## Features
- text coloring and styling
- item selection from lists
- progress bar