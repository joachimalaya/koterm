# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Added
- progress bar can be displayed with a status optionally
- constants for moving the cursor in the terminal with ANSI codes
- simple tables

### Changed
- progress bar is colored
- padding for indices of ordered lists

## 0.1.0
### Added
- project infrastructure
- text styling in terminals:
    - common colors
    - italic, bold, underline
    - framed, encircled, overlined
- display Lists as ordered and unordered lists in the terminal
- allows user to pick an item from a displayed list  (async/sync variants)
- terminal progress bar